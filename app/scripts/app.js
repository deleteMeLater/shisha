import svg4everybody from 'svg4everybody';
import $ from 'jquery';
import 'jquery.rd-navbar';
import 'swiper';
import 'jquery-ui/ui/widgets/accordion';
import '@zeitiger/elevatezoom/jquery.elevatezoom.js';
import 'fancybox/dist/js/jquery.fancybox';
import 'jquery.bxslider';
import 'jquery.cookie';
import enquire from 'enquire.js';

$(() => {
	svg4everybody();
	function addReview() {
		$('.detail__review-link, .detail__add-review-link').click(function (e) {
			e.preventDefault();
			$('.detail__tab-item').removeClass('active');
			$('.detail__tab-content').removeClass('active');
			$('.detail__tab-item:nth-of-type(2)').addClass('active');
			$('.detail__tab-content:nth-of-type(2)').addClass('active');
			$('html, body').animate({
				scrollTop: $('.detail__tab-caption').offset().top - ($('.rd-navbar-panel').outerHeight() + 50)
			}, 1000);
		});
	}
	function topButton() {
		const topShow = 150;
		const delay = 1000;
		$(window).scroll(function () {
			if ($(this).scrollTop() > topShow) {
				$('.top-button').fadeIn();
			}else {
				$('.top-button').fadeOut();
			}
		});
		$('.top-button').click(function () {
			$('body, html').animate({
				scrollTop: 0
			}, delay);
		});
	}
	// Активная ссылка меню
	function linkCheck() {
		const link = $('.main-menu__link--header');
		link.each(function () {
			if (this.href === location.href) {
				$(this).addClass('main-menu__link--active');
			}
		});
	}
	function tabs() {
		$('ul.home__tab-caption').on('click', 'li:not(.active)', function () {
			$(this)
				.addClass('active').siblings().removeClass('active')
				.closest('div.home__tab').find('div.home__tab-content').removeClass('active').eq($(this).index()).addClass('active');
		});
	}
	function detailTabs() {
		$('ul.detail__tab-caption').on('click', 'li:not(.active)', function () {
			$(this)
				.addClass('active').siblings().removeClass('active')
				.closest('div.detail__tab').find('div.detail__tab-content').removeClass('active').eq($(this).index()).addClass('active');
		});
	}
	$('.rd-navbar').RDNavbar();
	// Слайдер на главной
	// function slider() {
	// 	const s = $('.swiper-container');
	// 	function toggleSwiperCaptionAnimation(swiper) {
	// 		let a = $('.swiper__buy');
	// 		let activeSlide = $('.swiper-wrapper').find('.swiper-slide-active');
	// 		console.log(activeSlide.find('.swiper__buy').attr('href'));
	// 		a.removeClass('swiper-animated');
	// 		a.addClass('swiper-not-animated');
	// 		activeSlide.find('.swiper__buy').removeClass('swiper-not-animated');
	// 		activeSlide.find('.swiper__buy').addClass('swiper-animated');
	// 		// const prevSlide = $(swiper.container);
	// 		// const nextSlide = $(swiper.slides[swiper.activeIndex]);
	// 		// prevSlide
	// 		// 	.find('[data-caption-animate]')
	// 		// 	.each(function () {
	// 		// 		let $this = $(this);
	// 		// 		$this
	// 		// 			.removeClass('swiper-animated')
	// 		// 			.removeClass($this.attr('data-caption-animate'))
	// 		// 			.addClass('swiper-not-animated');
	// 		// 	});
	// 		// nextSlide
	// 		// 	.find('[data-caption-animate]')
	// 		// 	.each(function () {
	// 		// 		let $this = $(this);
	// 		// 		const delay = $this.attr('data-caption-delay');
	// 		// 		setTimeout(function () {
	// 		// 			$this
	// 		// 				.removeClass('swiper-not-animated')
	// 		// 				.addClass($this.attr('data-caption-animate'))
	// 		// 				.addClass('swiper-animated');
	// 		// 		}, delay);
	// 		// 	});
	// 	}
	// 	const mySwiper = new Swiper('.swiper-container', {
	// 		mode: 'horizontal',
	// 		autoplay: false, // s.attr('data-autoplay') ? s.attr('data-autoplay') === 'false' ? undefined : s.attr('data-autoplay') : 5000,
	// 		effect: 'fade', // s.attr('data-slide-effect') ? s.attr('data-slide-effect') : 'fade',
	// 		// speed: s.attr('data-slide-speed') ? s.attr('data-slide-speed') : 800,
	// 		// keyboardControl: s.attr('data-keyboard') === 'true',
	// 		// mousewheelControl: s.attr('data-mousewheel') === 'true',
	// 		// mousewheelReleaseOnEdges: s.attr('data-mousewheel-release') === 'true',
	// 		nextButton: '.swiper-button-next',
	// 		prevButton: '.swiper-button-prev',
	// 		preventClicks: false,
	// 		slidesPerView: 1,
	// 		// pagination: pag.length ? pag.get(0) : null,
	// 		// paginationClickable: pag.length ? pag.attr('data-clickable') !== 'false' : false,
	// 		// paginationBulletRender: pag.length ? pag.attr('data-index-bullet') === 'true' ? function (index, className) {
	// 		// 	return '<span class="' + className + '">' + (index + 1) + '</span>';
	// 		// } : null : null,
	// 		// scrollbar: (bar.length) ? bar.get(0) : null,
	// 		// scrollbarDraggable: bar.length ? bar.attr('data-draggable') !== 'false' : true,
	// 		// scrollbarSnapOnRelease: true,
	// 		// scrollbarHide: bar.length ? bar.attr('data-draggable') === 'false' : false,
	// 		// loop: s.attr('data-loop') !== 'false',
	// 		loop: false
	// 		onTransitionEnd: (swiper) => {
	// 			toggleSwiperCaptionAnimation(swiper);
	// 		}
	// 		// onInit: (swiper) => {
	// 		// 	toggleSwiperCaptionAnimation(swiper);
	// 		// }
	// 	});
	// }
	// Вся мишура с галереей товара в карточке товара
	function gallery() {
		const o = $('#productGallery');
		const o1 = $('#productZoom');
		const o2 = $('#productFullGallery');
		if (o.length) {
			o.bxSlider({
				mode: 'vertical',
				pager: false,
				controls: true,
				slideMargin: 13,
				minSlides: 4,
				maxSlides: 4,
				slideWidth: o.attr('data-slide-width') ? o.attr('data-slide-width') : undefined,
				nextText: '<svg width="10" height="10" class="detail__icon detail__icon--next"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/images/icon.svg#icon_arrow-down"></use></svg>',
				prevText: '<svg width="10" height="10" class="detail__icon detail__icon--prev"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/images/icon.svg#icon_arrow-down"></use></svg>',
				infiniteLoop: false,
				adaptiveHeight: true,
				moveSlides: 1,
				touchEnabled: false
			})
			.find('li:first-child > a').addClass('zoomGalleryActive');
		}
		if (o1.length) {
			o1.elevateZoom({
				gallery: 'productGallery',
				responsive: true,
				cursor: o1.data('zoom-type') === 1 ? 'crosshair' : 'pointer',
				zoomType: o1.data('zoom-type') === 1 ? 'inner' : (o1.data('zoom-type') === 2 || o1.data('zoom-type') === 3) ? 'lens' : undefined,
				lensShape: o1.data('zoom-type') === 2 ? 'round' : undefined,
				constrainType: o1.data('zoom-type') === 3 ? 'height' : undefined,
				ontainLensZoom: o1.data('zoom-type') === 3 ? true : undefined
			})
			.bind('click', function () {
				$.fancybox(o1.data('elevateZoom').getGalleryList());
				return false;
			});
		}
		if (o2.length) {
			$(document).ready(function () {
				o2.elevateZoom({
					gallery: 'productGallery',
					responsive: true,
					cursor: o2.data('zoom-type') === 1 ? 'crosshair' : 'pointer',
					zoomType: o2.data('zoom-type') === 1 ? 'inner' : (o1.data('zoom-type') === 2 || o2.data('zoom-type') === 3) ? 'lens' : undefined,
					lensShape: o2.data('zoom-type') === 2 ? 'round' : undefined,
					constrainType: o2.data('zoom-type') === 3 ? 'height' : undefined,
					containLensZoom: o2.data('zoom-type') === 3 ? true : undefined
				});
				o2
				.bxSlider({
					pager: false,
					controls: true,
					minSlides: 1,
					maxSlides: 1,
					infiniteLoop: false,
					moveSlides: 1,
					touchEnabled: false,
					nextText: '<svg width="10" height="10" class="detail__icon detail__icon--next"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/images/icon.svg#icon_arrow-down"></use></svg>',
					prevText: '<svg width="10" height="10" class="detail__icon detail__icon--prev"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/images/icon.svg#icon_arrow-down"></use></svg>'
				})
				.bind('click', function () {
					$.fancybox(o2.data('elevateZoom').getGalleryList());
					return false;
				});
			});
		}
	}
	// function quikviewSlider() {
	// 	let $this = $('.quikview__slider-item:first').addClass('quikview__slider-item--active');
	// 	$('.quikview__next-slide').click(function () {
	// 		$this.removeClass('quikview__slider-item--active');
	// 		$this = $this.next();
	// 		if (!($this.hasClass('quikview__slider-item'))) {
	// 			$this = $('.quikview__slider-item:first');
	// 		}
	// 		$this.addClass('quikview__slider-item--active');
	// 	});
	// 	$('.quikview__prev-slide').click(function () {
	// 		$this.removeClass('quikview__slider-item--active');
	// 		$this = $this.prev();
	// 		if (!($this.hasClass('quikview__slider-item'))) {
	// 			$this = $('.quikview__slider-item:last');
	// 		}
	// 		$this.addClass('quikview__slider-item--active');
	// 	});
	// }
	// Аккордион для добавки отзыва на детальной странице
	$('.review__add-review').accordion({
		collapsible: true,
		active: false,
		animate: 300,
		heightStyle: 'content'
	});
	// Аккордион в группе фильтров
	$('.filter__list-item').accordion({
		collapsible: true,
		active: false,
		animate: 300,
		heightStyle: 'content'
	});
	$('.checkout__item').accordion({
		collapsible: true,
		active: false,
		animate: 300,
		heightStyle: 'content'
	});
	// Анимация стрелочки у селекта
	$('.selectBox').click(function () {
		$(this).find('.sort__icon-arrow').toggleClass('sort__icon-arrow--active');
	});
	// Анимация стрелочки у аккордиона
	$('.filter__search, .filter__item').click(function () {
		$(this).find('.filter__icon').toggleClass('filter__icon--active');
	});
	// Прелоадер
	function preloader() {
		$(window).load(function (){
			$('.page__preloader').addClass('page__preloader--fadeout');
		});
	}
	function productShow() {
		$('.sort__button').click(function () {
			$('.sort__icon').removeClass('sort__icon--active');
			$(this).next('.sort__icon').addClass('sort__icon--active');
		});
	}
	// Аккордион в корзине
	$('.order-options__items').accordion({
		collapsible: true,
		active: false,
		animate: 300,
		heightStyle: 'content'
	});
	// Мобильное отображение страницы Блог
	function mobileBlog() {
		function toggleAcc() {
			return {
				match: () => {
					const icons = {
						header: 'blog__icon blog__icon--plus',
						activeHeader: 'blog__icon blog__icon--minus'
					};
					$('.blog__wrapper').accordion({
						collapsible: true,
						active: 0,
						animate: 300,
						heightStyle: 'content',
						icons: icons
					});
				},
				unmatch: () => {
					$('.blog__wrapper').accordion('destroy');
				}
			};
		}
		enquire
			.register('screen and (max-width: 767px)', toggleAcc());
	}
	function modalClose() {
		if (!$.cookie('was')) {
			$('.welcome').css('display', 'block');
			$('#welcome-overlay').css('display', 'block');
		}
		const modal = $('.welcome');
		const overlay = $('#welcome-overlay');
		const close = $('.welcome__button--green');
		close.click(function (e) {
			e.preventDefault();
			modal.animate({
				opacity: 0,
				top: '20%'
			}, 200, function () {
				$(this).css('display', 'none');
				overlay.fadeOut(300);
			});
			$.cookie('was', true, {
				expires: 1,
				path: '/'
			});
		});
	}
	// Слайдер на главной странице
	function slider() {
		const homeSlider = $('.swiper-container');
		function toggleSwiperCaptionAnimation(swiper) {
			const prevSlide = $(swiper.container);
			const nextSlide = $(swiper.slides[swiper.activeIndex]);
			prevSlide
				.find('[data-caption-animate]')
				.each(function () {
					const $this = $(this);
					$this
					.removeClass('swiper-animated')
					.removeClass($this.attr('data-caption-animate'))
					.addClass('swiper-not-animated');
				});
			nextSlide
				.find('[data-caption-animate]')
				.each(function () {
					const $this = $(this);
					setTimeout(function () {
						$this
						.removeClass('swiper-not-animated')
						.addClass($this.attr('data-caption-animate'))
						.addClass('swiper-animated');
					}, 300);
				});
		}
		homeSlider
			.find('.swiper__buy')
			.addClass('swiper-not-animated')
			.end()
			.swiper({
				effect: 'fade',
				nextButton: '.swiper-button-next',
				prevButton: '.swiper-button-prev',
				loop: true,
				onTransitionEnd: swiper => {
					toggleSwiperCaptionAnimation(swiper);
				},
				onInit: swiper => {
					toggleSwiperCaptionAnimation(swiper);
				}
			});
	}
	addReview();
	preloader();
	slider();
	productShow();
	mobileBlog();
	gallery();
	linkCheck();
	tabs();
	detailTabs();
	modalClose();
	topButton();
});
